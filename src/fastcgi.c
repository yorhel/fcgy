/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fcgy.h"


/* Returns -1 if no complete record has been received, otherwise writes to *h
 * and returns the length of the first record in the buffer. */
static ssize_t parse_header(fastcgi_header *h, char *buf, size_t buflen) {
    if(buflen < FCGI_HEADER_LEN)
        return -1;
    memcpy(h, buf, FCGI_HEADER_LEN);
    /* TODO: A proper byteswap in the struct itself seems more efficient and clear */
    h->requestId     = (((uint8_t)buf[2])<<8) + ((uint8_t)buf[3]);
    h->contentLength = (((uint8_t)buf[4])<<8) + ((uint8_t)buf[5]);

    size_t len = FCGI_HEADER_LEN + h->contentLength + h->paddingLength;
    return buflen < len ? -1 : (ssize_t)len;
}


static void reader_consume(fastcgi_reader *r) {
    size_t len = r->buf_len;
    char *buf = r->buf;

    r->processing = true;

    while(r->cb) {
        fastcgi_header h;
        ssize_t n = parse_header(&h, buf, len);
        if(n < 0)
            break;
        r->cb(r, n, h, buf+FCGI_HEADER_LEN);
        len -= n;
        buf += n;
    }
    memmove(r->buf, buf, len);
    r->buf_len = len;

    r->processing = false;
    if(!r->cb)
        fastcgi_reader_destroy(r);
}


static void reader_io_cb(EV_P_ ev_io *w, int revents) {
    fastcgi_reader *r = w->data;
    assert(r->cb != NULL);

    ssize_t n = read(r->fd, r->buf+r->buf_len, sizeof(r->buf)-r->buf_len);
    if(n < 0 && (errno == EINTR || errno == EWOULDBLOCK || errno == EAGAIN))
        return;

    if(n <= 0) {
        if(n == 0)
            errno = ECONNRESET;
        fastcgi_header h = {0};
        r->cb(r, -1, h, NULL);
        return;
    }

    r->buf_len += n;
    reader_consume(r);
}


fastcgi_reader *fastcgi_reader_create(int fd, fastcgi_reader_cb cb, void *data) {
    fastcgi_reader *r = malloc(sizeof(fastcgi_reader));
    r->fd = fd;
    r->buf_len = 0;
    r->data = data;
    r->cb = cb;
    ev_io_init(r->io, reader_io_cb, r->fd, EV_READ);
    r->io->data = r;
    return r;
}


void fastcgi_reader_destroy(fastcgi_reader *r) {
    r->cb = NULL;
    if(r->processing)
        return;
    ev_io_stop(EV_DEFAULT_UC_ r->io);
    free(r);
}




static ssize_t param_parse_len(fastcgi_param_parser *p, uint8_t c) {
    bool next = false;
    uint32_t *val   = p->state == 0 ? &p->namelen : &p->valuelen;
    uint32_t maxval = p->state == 0 ? 1<<10 : 1<<20;

    if(p->off == 0)
        *val = 0;

    if(p->off == 0 && !(c & 0x80)) { /* Single byte */
        *val = c;
        next = true;
    } else { /* 4 bytes */
        *val = p->off == 0 ? (c & ~0x80) : (*val << 8) + c;
        next = ++p->off == 4;
    }

    if(next) {
        p->state++;
        p->off = 0;
        if(p->namelen < 1 || *val > maxval)
            return -1;
    }
    return 0;
}


ssize_t fastcgi_param_parse(fastcgi_param_parser *p, const char *buf, size_t len) {
    /* TODO: Using memcpy() and memchr() for processing name/value data is
     * likely faster than this byte-for-byte copying. Also a bit more complex. */
    size_t r = 0;
    while(r < len) {
        char c = buf[r];
        r++;

        switch(p->state) {

        case 0: /* Name length */
        case 1: /* Value length */
            if(param_parse_len(p, c) < 0)
                return -1;
            if(p->state == 2)
                p->buf = malloc(p->namelen + p->valuelen + 2);
            break;

        case 2: /* Name data */
            if(c == 0 || c == '=') {
                free(p->buf);
                p->buf = NULL;
                return -1;
            }
            p->buf[p->off++] = c;
            if(p->off == p->namelen) {
                p->buf[p->off++] = 0;
                p->state++;
                if(p->valuelen == 0) {
                    p->buf[p->off] = 0;
                    p->state = 0;
                    p->off = 0;
                    return r;
                }
            }
            break;

        case 3: /* Value data */
            if(c == 0) {
                free(p->buf);
                p->buf = NULL;
                return -1;
            }
            p->buf[p->off++] = c;
            if(p->off == p->namelen + p->valuelen + 1) {
                p->buf[p->off] = 0;
                p->state = 0;
                p->off = 0;
                return r;
            }
            break;
        }
    }
    return 0;
}
