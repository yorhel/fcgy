/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fcgy.h"


/* Only sets the flags, can't unset anything currently. */
int util_fd_flags(int fd, uint32_t flags) {
    int r;

    if(flags & FCGY_NONBLOCK)
        if((r = fcntl(fd, F_GETFL)) < 0 || fcntl(fd, F_SETFL, r|O_NONBLOCK) < 0)
            return -1;

    if(flags & FCGY_CLOEXEC)
        if((r = fcntl(fd, F_GETFD)) < 0 || fcntl(fd, F_SETFD, r|FD_CLOEXEC) < 0)
            return -1;

    return 0;
}


int util_serversock(int domain, void *addr, socklen_t addrlen, uint32_t flags) {
    int fd = socket(domain, SOCK_STREAM, 0);
    if(fd < 0)
        return fd;

    int r = 1;
#ifdef IPV6_V6ONLY
    /* Can fail, ignore error */
    setsockopt(fd, IPPROTO_IPV6, IPV6_V6ONLY, &r, sizeof(int));
#endif

    if(util_fd_flags(fd, flags) < 0
    || setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &r, sizeof(int)) < 0
    || bind(fd, addr, addrlen) < 0
    || listen(fd, 64) < 0) {
        close(fd);
        return -1;
    }
    return fd;
}


int util_parse_addr(const char *a, util_sockaddr *sock) {
    char buf[128];
    int n;
    unsigned short port = 0;

    memset(sock, 0, sizeof(util_sockaddr));
    if(strcmp(a, "-") == 0)
        return 0;

    /* IPv6 */
    if(sscanf(a, "[%127[a-fA-Z0-9:]]:%hu%n", buf, &port, &n) == 2 && (size_t)n == strlen(a)) {
        sock->in6.sin6_family = AF_INET6;
        sock->in6.sin6_port = htons(port);
        if(inet_pton(AF_INET6, buf, &sock->in6.sin6_addr) != 1)
            return -1;
        return 0;
    }

    /* IPv4 */
    if(sscanf(a, "%127[0-9.]:%hu%n", buf, &port, &n) == 2 && (size_t)n == strlen(a)) {
        sock->in.sin_family = AF_INET;
        sock->in.sin_port = htons(port);
        if(inet_pton(AF_INET, buf, &sock->in.sin_addr) != 1)
            return -1;
        return 0;
    }

    /* port */
    if(sscanf(a, "%hu%n", &port, &n) == 1 && (size_t)n == strlen(a)) {
        /* We already zero-initialze the sockaddr struct, so the 0.0.0.0 address is set */
        sock->in.sin_family = AF_INET;
        sock->in.sin_port = htons(port);
        return 0;
    }

    /* UNIX socket path */
    if(*a == '/') {
        sock->un.sun_family = AF_UNIX;
        n = snprintf(sock->un.sun_path, sizeof(sock->un.sun_path), "%s", a);
        if(n < 0 || (size_t)n >= sizeof(sock->un.sun_path))
            return -1;
        return 0;
    }

    return -1;
}


void util_format_addr(const util_sockaddr *s, char *dst) {
    switch(s->generic.sa_family) {
    case 0:
        strcpy(dst, "-");
        break;
    case AF_INET:
        inet_ntop(AF_INET, &s->in.sin_addr, dst, UTIL_ADDRSTRLEN);
        snprintf(dst+strlen(dst), UTIL_ADDRSTRLEN-strlen(dst), ":%"PRIu16, ntohs(s->in.sin_port));
        break;
    case AF_INET6:
        *dst = '[';
        inet_ntop(AF_INET6, &s->in6.sin6_addr, dst+1, UTIL_ADDRSTRLEN-1);
        snprintf(dst+strlen(dst), UTIL_ADDRSTRLEN-strlen(dst), "]:%"PRIu16, ntohs(s->in6.sin6_port));
        break;
    case AF_UNIX:
        strcpy(dst, s->un.sun_path);
        break;
    }
}



typedef vec_t(char) util_buf; /* Abusing vec as a string library */

static int unquote_double(const char *orig, util_buf *buf) {
    const char *str = orig;
    str++;
    while(*str) {
        switch(*str) {
        case '"':  return str-orig+1;
        case '\\':
            str++;
            switch(*str) {
            case '\0':
                return -1;
            case '"':
            case '\\':
            case '`':
            case '$':
            case '\n':
                vec_append(*buf, *str);
                break;
            default:
                vec_append(*buf, '\\');
                str--; /* Didn't mean to escape this char */
            }
            break;
        default:
            vec_append(*buf, *str);
        }
        str++;
    }
    return -1;
}


static int unquote_single(const char *orig, util_buf *buf) {
    const char *str = orig;
    str++;
    while(*str) {
        if(*str == '\'')
            return str-orig+1;
        vec_append(*buf, *str);
        str++;
    }
    return -1;
}


void util_free_argv(util_argv *arg) {
    size_t i;
    for(i=0; i<arg->n; i++)
        free(arg->a[i]);
    vec_clear(*arg);
}


int util_parse_argv(const char *str, util_argv *arg) {
    memset(arg, 0, sizeof(util_argv));
    util_buf buf = {0};
    int n;

    while(*str) {
        switch(*str) {
        case ' ':
        case '\t':
        case '\n':
            if(buf.a) {
                vec_append(buf, 0);
                vec_append(*arg, buf.a);
                memset(&buf, 0, sizeof(buf));
            } else
                vec_clear(buf);
            break;
        case '\\':
            str++;
            switch(*str) {
            case '\n': break;
            case '\0':
                free(buf.a);
                util_free_argv(arg);
                return -1;
            default:
                vec_append(buf, *str);
            }
            break;
        case '"':
        case '\'':
            if(!buf.a) /* Make sure we create a token, even if it's an empty string */
                vec_grow(buf);
            n = (*str == '"' ? unquote_double : unquote_single)(str, &buf);
            if(n < 0) {
                free(buf.a);
                util_free_argv(arg);
                return -1;
            }
            str += n-1;
            break;
        default:
            vec_append(buf, *str);
        }
        str++;
    }

    if(buf.a) {
        vec_append(buf, 0);
        vec_append(*arg, buf.a);
    }
    vec_append(*arg, NULL);
    return 0;
}
