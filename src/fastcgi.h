/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef FCGY_FASTCGI_H
#define FCGY_FASTCGI_H


#define FCGI_BEGIN_REQUEST       1
#define FCGI_ABORT_REQUEST       2
#define FCGI_END_REQUEST         3
#define FCGI_PARAMS              4
#define FCGI_STDIN               5
#define FCGI_STDOUT              6
#define FCGI_STDERR              7
#define FCGI_DATA                8
#define FCGI_GET_VALUES          9
#define FCGI_GET_VALUES_RESULT  10
#define FCGI_UNKNOWN_TYPE       11
#define FCGI_MAXTYPE (FCGI_UNKNOWN_TYPE)

#define FCGI_HEADER_LEN  8

typedef struct {
    uint8_t  version;
    uint8_t  type;
    uint16_t requestId;
    uint16_t contentLength;
    uint8_t  paddingLength;
    uint8_t  reserved;
} fastcgi_header;

typedef struct fastcgi_reader fastcgi_reader;

/* Called when a complete FastCGI record has arrived. Arguments:
 *   ssize_t -> -1 on error (see errno), otherwise total length of the record.
 *              (XXX: Is the total length useful?)
 *   char *  -> buffer containing the record contents (after the header)
 */
typedef void(*fastcgi_reader_cb)(fastcgi_reader *, ssize_t, fastcgi_header, const char *);

struct fastcgi_reader {
    int fd;
    bool processing; /* Currently processing the buffer */
    ev_io io[1];
    fastcgi_reader_cb cb; /* Set to NULL when _destroy()'ed while processing */
    void *data;
    size_t buf_len;
    /* Read buffer. Large enough to hold a single FastCGI record.
     * (should this be a dynamic buffer? Might save some memory and/or optimize read() calls) */
    char buf[8+65535+255];
};

fastcgi_reader *fastcgi_reader_create(int, fastcgi_reader_cb, void *);
void fastcgi_reader_destroy(fastcgi_reader *);
static inline void fastcgi_reader_start(fastcgi_reader *r) { ev_io_start(EV_DEFAULT_UC_ r->io); }
static inline void fastcgi_reader_stop (fastcgi_reader *r) { ev_io_stop (EV_DEFAULT_UC_ r->io); }




typedef struct {
    uint32_t state;
    uint32_t namelen;
    uint32_t valuelen;
    uint32_t off; /* Offset into the current 4-byte length specification or in *buf */
    char *buf;
} fastcgi_param_parser;

/* Consumes the first name/value pair from the given buffer. The parser state
 * should be zero-initialized before the first call to this function and before
 * re-using the state struct after an error.
 *
 * Returns 0 and updates its internal state if no complete pair is in the
 * buffer. It can then be called again later with the continuation of the
 * buffer.
 *
 * Returns -1 on error. The following things are considered an error:
 * - Name contains '='
 * - Name or value contains \0
 * - Name is zero-length
 * - Name is larger than 1 KiB
 * - Value is larger than 1 MiB
 * (The length limits are completely arbitrary, but 2 GiB param names/values
 * are a security nightmare waiting to happen)
 *
 * On success it returns the number of bytes consumed by the first pair in the
 * given buffer (i.e. the number of bytes you can skip to get to the start of
 * the next pair). The *buf field then points to a newly allocated buffer
 * starting with the \0-terminated name and followed by the \0-terminated
 * value. Ownership of this buffer is passed to the user. The namelen and
 * valuelen fields are equivalent to a strlen() on the name and value.
 * (Redundant note: The value starts at buf+namelen+1)
 */
ssize_t fastcgi_param_parse(fastcgi_param_parser *, const char *, size_t);

#endif
