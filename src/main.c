/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <fcgy.h>
#include <yopt.h>

static fcgy_app *app; /* TODO: Needs to be a global array of apps */
static ev_signal termsig, intsig;


static const yopt_opt_t cli_options[] = {
#define X(N, n) { FCGY_CONFIG_GLOBAL_##N, 1, "--"n },
    FCGY_CONFIG_GLOBAL
#undef X
#define X(N, n) { FCGY_CONFIG_APP_##N, 1, "--"n },
    FCGY_CONFIG_APP
#undef X
#define X(N, n, a) { FCGY_CONFIG_CLI_##N, a, n },
    FCGY_CONFIG_CLI
#undef X
    {0,0,NULL}
};


static int parse_args(int argc, char **argv) {
    int v;
    char *val;
    yopt_t opts;
    yopt_init(&opts, argc, argv, cli_options);

    while((v = yopt_next(&opts, &val)) >= 0) {
        if(FCGY_CONFIG_IS_APP(v)) {
            if(app_config(app, v, val, opts.errbuf, sizeof(opts.errbuf)) < 0) {
                fprintf(stderr, "fcgy: %s\n", opts.errbuf);
                return -1;
            }
            continue;
        }
        switch(v) {
        case FCGY_CONFIG_GLOBAL_LOG_LEVEL:
            ylog_set_level(2, val);
            break;
        case FCGY_CONFIG_CLI_VERSION:
            printf("fcgy %s\n", fcgy_version);
            exit(0);
        case FCGY_CONFIG_CLI_HELP:
            /* Very helpful */
            printf("fcgy [options]\n");
            exit(0);
        default:
            assert(0 && "Unhandled option");
        }
    }

    if(v == -2) {
        fprintf(stderr, "fcgy: %s\n", val);
        return -1;
    }

    if(!app->fronts) {
        fprintf(stderr, "No front-ends configured.\n");
        return -1;
    }
    if(!app->back) {
        fprintf(stderr, "No backend configured.\n");
        return -1;
    }
    return 0;
}


static void shutdown_sig(EV_P_ ev_signal *w, int revents) {
    ev_signal_stop(EV_DEFAULT_UC_ &termsig);
    ev_signal_stop(EV_DEFAULT_UC_ &intsig);
    app_destroy(app);
}


int main(int argc, char **argv) {
    ylog_set_level(2, "2");
    /* TODO: Set a custom ylog handler (writing to stderr while we have a console, syslog/file otherwise) */

    app = app_create();
    if(parse_args(argc, argv) < 0)
        return 1;

    ev_default_loop(0);

    char ebuff[256];
    if(app_bind(app, ebuff, sizeof(ebuff)) < 0) {
        fprintf(stderr, "fcgy: %s\n", ebuff); /* XXX: Probably better to let app_bind() call yerr() instead of this indirection */
        app_destroy(app);
        return 1;
    }
    if(app_run(app) < 0) {
        app_destroy(app);
        return 1;
    }

    ev_signal_init(&termsig, shutdown_sig, SIGTERM);
    ev_signal_init(&intsig, shutdown_sig, SIGINT);
    ev_signal_start(EV_DEFAULT_UC_ &termsig);
    ev_signal_start(EV_DEFAULT_UC_ &intsig);

    yinfo("%s %s started", PACKAGE_NAME, fcgy_version);
    ev_run(EV_DEFAULT_UC_ 0);
    yinfo("Shutting down");
    return 0;
}
