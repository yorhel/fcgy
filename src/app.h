/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef FCGY_APP_H
#define FCGY_APP_H

enum fcgy_front_type {
    FCGY_FRONT_FASTCGI
};


struct fcgy_front {
    fcgy_front *next, *prev;
    fcgy_app *app;

    fcgy_front_type type;
    int fd;
    util_sockaddr addr;
    ev_io listener[1];
    ev_timer backoff[1];
};

struct fcgy_app {
    fcgy_front *fronts;      /* Linked list of frontends */
    fconn_fastcgi *fconns;   /* Linked list of (alive) frontend connections */
    /* TODO: uid / gid */
    back_fastcgi *back;      /* Only a single (FastCGI) backend supported for now */
    char *chroot;
};

fcgy_app *app_create();
int app_config(fcgy_app *, fcgy_config_name, const char *, char *, size_t);
int app_bind(fcgy_app *, char *, size_t);
int app_run(fcgy_app *);
void app_destroy(fcgy_app *);

#endif
