/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fcgy.h"

/* Comparison for use with the c->reqs vector */
#define req_cmp(c, val) (((int)(c)->reqs.a[i]->fconn_id) - ((int)val))


static void handle_get_values(fconn_fastcgi *c, fcgy_req *req, fastcgi_header h, const char *buf) {
    /* TODO */
}


static void handle_begin_request(fconn_fastcgi *c, fcgy_req *req, fastcgi_header h, const char *buf) {
    if(h.requestId == 0 || h.contentLength < 8 || *buf != 0 || buf[1] < 1 || buf[1] > 3) {
        ywarn("Invalid BEGIN_REQUEST record, disconnecting front-end.");
        fconn_fastcgi_destroy(c);
        return;
    }

    size_t n;
    vec_search_insert(c->reqs, n, req_cmp(c, h.requestId));
    if(n < c->reqs.n && c->reqs.a[n]->fconn_id == h.requestId) {
        ywarn("BEGIN_REQUEST record for existing request ID, disconnecting front-end.");
        fconn_fastcgi_destroy(c);
        return;
    }
    *vec_insert_orderp(c->reqs, n) = req_create(c, h.requestId);
}


static void handle_abort_request(fconn_fastcgi *c, fcgy_req *req, fastcgi_header h, const char *buf) {
    req_unset_front(req);
    vec_search(c->reqs, req_cmp(c, h.requestId), vec_remove_order(c->reqs, i));
}


static void handle_params(fconn_fastcgi *c, fcgy_req *req, fastcgi_header h, const char *buf) {
    size_t len = h.contentLength;
    if(!len) /* End of param list. Should signal this to fcgy_req? */
        return;

    ssize_t n;
    while(len > 0) {
        n = fastcgi_param_parse(c->pp, buf, len);
        if(n < 0) {
            ywarn("Invalid parameter, disconnecting front-end.");
            fconn_fastcgi_destroy(c);
            return;
        }
        if(n == 0)
            break;

        req_set_param(req, c->pp->namelen, c->pp->valuelen, c->pp->buf);
        c->pp->buf = NULL; /* To avoid a double-free in fconn_fastcgi_destroy() */
        len -= n;
        buf += n;
    }
}


static void handle_data(fconn_fastcgi *c, fcgy_req *req, fastcgi_header h, const char *buf) {
    /* TODO */
}


static void read_cb(fastcgi_reader *r, ssize_t len, fastcgi_header h, const char *buf) {
    fconn_fastcgi *c = r->data;

    if(len < 0) {
        if(errno != ECONNRESET || c->reqs.n)
            ywarn("Unexpected connection error: %s", strerror(errno));
        fconn_fastcgi_destroy(c);
        return;
    }

    ytrace("Got FastCGI record: len = %4u, version = %u, type = %2u, requestId = %2u, contentLength = %4u\n",
        (unsigned)len, (unsigned)h.version, (unsigned)h.type, (unsigned)h.requestId, (unsigned)h.contentLength);

    static const struct {
        uint8_t type;
        bool req;        /* Must have an existing request object */
        void (*cb)(fconn_fastcgi *, fcgy_req *, fastcgi_header, const char *);
    } *handler = NULL, types[] = {
        { FCGI_GET_VALUES,    false, handle_get_values    },
        { FCGI_BEGIN_REQUEST, false, handle_begin_request },
        { FCGI_ABORT_REQUEST, true,  handle_abort_request },
        { FCGI_PARAMS,        true,  handle_params        },
        { FCGI_STDIN,         true,  handle_data          },
        { FCGI_DATA,          true,  handle_data          }
    };

    size_t i;
    for(i=0; i<sizeof(types)/sizeof(*types); i++)
        if(types[i].type == h.type) {
            handler = types+i;
            break;
        }

    if(!handler) {
        ydebug("Received record with unknown type %u\n", (unsigned)h.type);
        /* TODO: Reply with FCGI_UNKNOWN_TYPE */
        return;
    }

    fcgy_req *req = NULL;
    if(handler->req) {
        vec_search(c->reqs, req_cmp(c, h.requestId), req = c->reqs.a[i]);
        if(!req) {
            ywarn("Received record for unknown request id (type = %u, id = %u), disconnecting front-end.\n", (unsigned)h.type, (unsigned)h.requestId);
            fconn_fastcgi_destroy(c);
            return;
        }
    }

    handler->cb(c, req, h, buf);
}


void fconn_fastcgi_create(fcgy_app *app, int rfd, int wfd) {
    fconn_fastcgi *c = calloc(1, sizeof(fconn_fastcgi));
    c->app = app;
    c->rfd = rfd;
    c->wfd = wfd;

    c->rd = fastcgi_reader_create(c->rfd, read_cb, c);
    fastcgi_reader_start(c->rd);

    hlist_prepend(app->fconns, c);
}


void fconn_fastcgi_destroy(fconn_fastcgi *c) {
    fastcgi_reader_destroy(c->rd);
    hlist_remove(c->app->fconns, c);

    free(c->pp->buf);

    size_t i;
    for(i=0; i<c->reqs.n; i++)
        req_unset_front(c->reqs.a[i]);
    free(c->reqs.a);

    close(c->wfd);
    if(c->wfd != c->rfd)
        close(c->rfd);

    free(c);
}
