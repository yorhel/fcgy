/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef FCGY_BACK_FASTCGI_H
#define FCGY_BACK_FASTCGI_H

/*
struct bconn_fastcgi {
    int fd;
    fcgy_req *req;
};

struct back_process {
    util_sockaddr addr;
    struct bconn_fastcgi *conns;
    int pid;
    int errfd, outfd;
};
*/

typedef struct back_fastcgi back_fastcgi;

struct back_fastcgi {
    fcgy_app *app;
    util_argv argv;
    char *chdir;
};


back_fastcgi *back_fastcgi_create(fcgy_app *, const char *);
int back_fastcgi_run(back_fastcgi *);
void back_fastcgi_destroy(back_fastcgi *);

#endif
