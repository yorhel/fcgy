/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fcgy.h"

back_fastcgi *back_fastcgi_create(fcgy_app *app, const char *cmd) {
    util_argv arg;
    if(util_parse_argv(cmd, &arg) < 0 || arg.n < 2)
        return NULL;

    back_fastcgi *b = calloc(1, sizeof(back_fastcgi));
    b->app = app;
    b->argv = arg;
    return b;
}


static void set_chdir(back_fastcgi *b) {
    if(b->chdir)
        return;
    char *file = strrchr(b->argv.a[0], '/');
    if(file) {
        char *oldcmd = b->argv.a[0];
        *file = 0;
        b->argv.a[0] = strdup(file+1);
        b->chdir = *oldcmd ? strdup(oldcmd) : NULL;
        free(oldcmd);
    }
}


int back_fastcgi_run(back_fastcgi *b) {
    set_chdir(b);
    if(b->chdir && chdir(b->chdir) < 0) {
        yerr("Could not chdir to '%s': %s", b->chdir, strerror(errno));
        return -1;
    }
    return 0;
}


/* XXX: Can't instantly destroy when we have processes running. Need async shutdown. */
void back_fastcgi_destroy(back_fastcgi *b) {
    free(b->chdir);
    util_free_argv(&b->argv);
    free(b);
}
