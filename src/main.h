/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef FCGY_MAIN_H
#define FCGY_MAIN_H

/* Global options.
 * X(NAME, "name") */
#define FCGY_CONFIG_GLOBAL\
    X(LOG_LEVEL, "log-level")

/* Application options.
 * X(NAME, "name") */
#define FCGY_CONFIG_APP \
    X(CHROOT,         "chroot")\
    X(FASTCGI,        "fastcgi")\
    X(FASTCGI_CHDIR,  "fastcgi-chdir")\
    X(LISTEN,         "listen")

/* Command-line options.
 * X(NAME, "yopt-style-name", needarg) */
#define FCGY_CONFIG_CLI \
    X(VERSION, "-V,--version", 0)\
    X(HELP,    "-h,--help", 0)

typedef enum {
    FCGY_CONFIG_GLOBAL_MARKER = 0x1000,
#define X(N, n) FCGY_CONFIG_GLOBAL_##N,
    FCGY_CONFIG_GLOBAL
#undef X
    FCGY_CONFIG_APP_MARKER = 0x2000,
#define X(N, n) FCGY_CONFIG_APP_##N,
    FCGY_CONFIG_APP
#undef X
    FCGY_CONFIG_CLI_MARKER = 0x4000,
#define X(N, n, a) FCGY_CONFIG_CLI_##N,
    FCGY_CONFIG_CLI
#undef X
    FCGY_CONFIG_END_MARKER
} fcgy_config_name;

#define FCGY_CONFIG_IS_APP(n) ((int)(n) & (int)FCGY_CONFIG_APP_MARKER)

#endif
