/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fcgy.h"

/* Time to wait before retrying a failed accept() */
#define LISTEN_BACKOFF_TIME 3.


static void front_timer_cb(EV_P_ ev_timer *w, int revents) {
    fcgy_front *f = w->data;
    ev_io_start(EV_A_ f->listener);
}


static void front_accept_cb(EV_P_ ev_io *w, int revents) {
    fcgy_front *f = w->data;

    int fd = accept(w->fd, NULL, NULL);
    if(fd < 0 && (errno == EINTR || errno == EWOULDBLOCK || errno == EAGAIN))
        return;

    if(util_fd_flags(fd, FCGY_NONBLOCK|FCGY_CLOEXEC) < 0) {
        close(fd);
        fd = -1;
    }

    if(fd < 0) {
        ywarn("Error accept()'ing: %s. Retrying in %.1f seconds.", strerror(errno), LISTEN_BACKOFF_TIME);
        ev_io_stop(EV_A_ w);
        ev_timer_set(f->backoff, LISTEN_BACKOFF_TIME, 0.);
        ev_timer_start(EV_A_ f->backoff);
        return;
    }

    /* TODO: Honor FCGI_WEB_SERVER_ADDRS, if set and this is an stdio frontend */

    fconn_fastcgi_create(f->app, fd, fd);
    ydebug("Incoming connection.");
}


/* TODO: Error reporting */
static fcgy_front *front_create(fcgy_app *app, const char *addr) {
    util_sockaddr a;
    if(util_parse_addr(addr, &a) < 0)
        return NULL;

    fcgy_front *f = calloc(1, sizeof(fcgy_front));
    memcpy(&f->addr, &a, sizeof(a));
    f->app = app;
    f->fd = -1;
    f->type = FCGY_FRONT_FASTCGI;
    ev_init(f->listener, front_accept_cb);
    ev_init(f->backoff, front_timer_cb);
    f->listener->data = f;
    f->backoff->data = f;
    return f;
}


static int front_bind(fcgy_front *f, char *err, size_t errlen) {
    if(f->addr.generic.sa_family == 0) {
        f->fd = 0;
        if(util_fd_flags(f->fd, FCGY_NONBLOCK|FCGY_CLOEXEC) < 0) {
            snprintf(err, errlen, "can't setup socket: %s", strerror(errno));
            return -1;
        }
        return 0;
    }

    f->fd = util_serversock(f->addr.generic.sa_family, &f->addr, util_sockaddr_len(f->addr.generic.sa_family), FCGY_CLOEXEC|FCGY_NONBLOCK);
    if(f->fd < 0) {
        snprintf(err, errlen, "can't bind socket: %s", strerror(errno));
        return -1;
    }
    return 0;
}


static void front_start(fcgy_front *f) {
    ev_io_set(f->listener, f->fd, EV_READ);
    ev_io_start(EV_DEFAULT_UC_ f->listener);
}


static void front_stop(fcgy_front *f) {
    if(f->fd < 0)
        return;

    if(f->addr.generic.sa_family == AF_UNIX)
        unlink(f->addr.un.sun_path);

    ev_io_stop(EV_DEFAULT_UC_ f->listener);
    ev_timer_stop(EV_DEFAULT_UC_ f->backoff);

    close(f->fd);
    f->fd = -1;
}


fcgy_app *app_create() {
    fcgy_app *app = calloc(1, sizeof(fcgy_app));
    return app;
}


/* Returns 0 on success, -1 on error.
 * Writes a string to *err on error. */
int app_config(fcgy_app *app, fcgy_config_name name, const char *val, char *err, size_t errlen) {
    fcgy_front *f;
    switch(name) {

    case FCGY_CONFIG_APP_CHROOT:
        free(app->chroot);
        app->chroot = strcmp(val, "no") == 0 ? NULL : strdup(val);
        return 0;

    case FCGY_CONFIG_APP_LISTEN:
        f = front_create(app, val);
        if(!f) {
            snprintf(err, errlen, "invalid address '%s'", val);
            return -1;
        }
        hlist_prepend(app->fronts, f);
        return 0;

    case FCGY_CONFIG_APP_FASTCGI:
        if(app->back) {
            snprintf(err, errlen, "multiple backends specified, only a single backend is supported");
            return -1;
        }
        app->back = back_fastcgi_create(app, val);
        if(!app->back) {
            snprintf(err, errlen, "invalid backend command '%s'", val);
            return -1;
        }
        return 0;

    case FCGY_CONFIG_APP_FASTCGI_CHDIR:
        if(!app->back) {
            snprintf(err, errlen, "no FastCGI backend configured");
            return -1;
        }
        free(app->back->chdir);
        app->back->chdir = strcmp(val, "no") == 0 ? NULL : strdup(val);
        return 0;

    default:
        assert(0 && "app_config() called on invalid option");
    }
    return -1;
}


int app_bind(fcgy_app *app, char *err, size_t errlen) {
    fcgy_front *f;
    char ebuff[256], addrbuf[UTIL_ADDRSTRLEN];
    for(f=app->fronts; f; f=f->next)
        if(front_bind(f, ebuff, sizeof(ebuff)) < 0) {
            util_format_addr(&f->addr, addrbuf);
            snprintf(err, errlen, "%s: %s", addrbuf, ebuff);
            return -1;
        }
    return 0;
}


int app_run(fcgy_app *app) {
    if(app->chroot && chroot(app->chroot) < 0) {
        yerr("Could not chroot to '%s': %s", app->chroot, strerror(errno));
        return -1;
    }

    fcgy_front *f;
    for(f=app->fronts; f; f=f->next)
        front_start(f);

    return back_fastcgi_run(app->back);
}


void app_destroy(fcgy_app *app) {
    while(app->fronts) {
        fcgy_front *n = app->fronts;
        hlist_remove(app->fronts, n);
        front_stop(n);
        free(n);
    }
    while(app->fconns)
        fconn_fastcgi_destroy(app->fconns);
    if(app->back)
        back_fastcgi_destroy(app->back);
    free(app);
}
