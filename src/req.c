/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fcgy.h"

fcgy_req *req_create(fconn_fastcgi *fconn, uint16_t id) {
    fcgy_req *r = calloc(1, sizeof(fcgy_req));
    r->front = fconn;
    r->fconn_id = id;
    return r;
}

void req_unset_front(fcgy_req *r) {
    r->front = NULL;

    /* No backends implemented yet. The free() needs to be deferred until both
     * front and back are NULL */
    assert(!r->back);

    size_t i;
    for(i=0; i<r->params.n; i++)
        free(r->params.a[i].buf);
    free(r->params.a);
    free(r);
}

void req_set_param(fcgy_req *r, uint32_t namelen, uint32_t valuelen, char *buf) {
    fcgy_req_param *p = vec_appendp(r->params);
    p->namelen = namelen;
    p->valuelen = valuelen;
    p->buf = buf;
    ytrace("Parameter: %s=%s\n", buf, buf+namelen+1);
}
