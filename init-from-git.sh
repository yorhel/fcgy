#!/bin/sh

set -x


aclean() {
    rm -f lean.m4
    hg clone https://bitbucket.org/GregorR/autoconf-lean
    make -C autoconf-lean\
        && mv autoconf-lean/lean.m4 .\
        && rm -rf autoconf-lean
}


ev() {
    d=libev-4.20
    rm -rf ev
    wget -q http://dist.schmorp.de/libev/Attic/$d.tar.gz\
        && tar -xzf $d.tar.gz\
        && rm -f $d.tar.gz\
        && mv $d ev
}


ylib() {
    U=http://g.blicky.net/ylib.git/plain
    rm -rf ylib
    mkdir -p ylib\
        && wget -q $U/list.h -O ylib/list.h\
        && wget -q $U/vec.h -O ylib/vec.h\
        && wget -q $U/ylog.c -O ylib/ylog.c\
        && wget -q $U/ylog.h -O ylib/ylog.h\
        && wget -q $U/yopt.h -O ylib/yopt.h
}

rm -r deps
mkdir -p deps
cd deps

aclean
ev
ylib
