/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fcgy.h"

#undef NDEBUG
#include <assert.h>

#define T(in, ...) do {\
        char *res[] = {__VA_ARGS__};\
        util_argv arg;\
        assert(util_parse_argv(in, &arg) == 0);\
        assert(arg.n-1 == sizeof(res)/sizeof(*res));\
        size_t i;\
        for(i=0; i<arg.n-1; i++)\
            assert(strcmp(arg.a[i], res[i]) == 0);\
        assert(arg.a[i] == NULL);\
        util_free_argv(&arg);\
        assert(arg.n == 0);\
        assert(arg.m == 0);\
        assert(arg.a == NULL);\
    } while(0)

#define F(in) do {\
        util_argv arg;\
        assert(util_parse_argv(in, &arg) == -1);\
        assert(arg.n == 0);\
        assert(arg.m == 0);\
        assert(arg.a == NULL);\
    } while(0)

int main(int argc, char **argv) {
    T("");
    T("   \t \n ");
    T("cmd arg1  arg2 \targ3\narg\\ 4", "cmd", "arg1", "arg2", "arg3", "arg 4");
    T("'' \"\"", "", "");
    T("'x'\\'y\\\"\"z\"", "x'y\"z");
    T("  cmd \"arg1\" 'arg2'  ", "cmd", "arg1", "arg2");
    T("\"a'r$g\"", "a'r$g");
    T("\\\"a\\'r$g\\\"", "\"a'r$g\"");
    T("'x\"y\\'", "x\"y\\");
    T("x \\\ny", "x", "y");
    T("' x y '\"  z\n\"", " x y   z\n");
    T("\\ \\ ", "  ");
    T("\"\\x\"", "\\x");

    F("'");
    F(" '' \"");
    F("\\");
    F("' xyz");
    F("\" xyz");
    F("xyz\\");
    F("xyz\"");
    return 0;
}
