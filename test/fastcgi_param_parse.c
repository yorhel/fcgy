/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fcgy.h"

#undef NDEBUG
#include <assert.h>

#define T(in, nl, vl, b) do {\
        fastcgi_param_parser p = {0};\
        assert(fastcgi_param_parse(&p, in, sizeof(in)-1) == sizeof(in)-1);\
        assert(p.namelen == nl);\
        assert(p.valuelen == vl);\
        assert(memcmp(p.buf, b, sizeof(b)-1) == 0);\
        assert(p.state == 0);\
        assert(p.off == 0);\
        free(p.buf);\
        size_t off = 0;\
        for(; off<sizeof(in)-2; off++)\
            assert(fastcgi_param_parse(&p, in+off, 1) == 0);\
        assert(fastcgi_param_parse(&p, in+off, 1) == 1);\
        assert(p.namelen == nl);\
        assert(p.valuelen == vl);\
        assert(memcmp(p.buf, b, sizeof(b)-1) == 0);\
        assert(p.state == 0);\
        assert(p.off == 0);\
        free(p.buf);\
    } while(0)

#define F(in) do {\
        fastcgi_param_parser p = {0};\
        assert(fastcgi_param_parse(&p, in, sizeof(in)-1) == -1);\
        assert(p.buf == NULL);\
        p.state = 0;\
        p.off = 0;\
        size_t off = 0;\
        ssize_t n = 0;\
        for(; off<sizeof(in)-1; off++)\
            if((n = fastcgi_param_parse(&p, in+off, 1)) != 0)\
                break;\
        assert(n == -1);\
        assert(p.buf == NULL);\
    } while(0)

#define x127 "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
#define x255 x127 x127 "x"

int main(int argc, char **argv) {
    T("\013\002SERVER_PORT80", 11, 2, "SERVER_PORT\080\0");
    T("\x80\0\0\1\x80\0\0\1XY", 1, 1, "X\0Y\0");
    T("\1\0x", 1, 0, "x\0\0");
    T("\1\1xx", 1, 1, "x\0x\0");
    T("\1\x80\0\0\0x", 1, 0, "x\0\0");
    T("\x80\0\0\1\1xx", 1, 1, "x\0x\0");
    T("\1\x7fy"x127, 1, 127, "y\0"x127"\0");
    T("\x7f\0"x127, 127, 0, x127"\0\0");
    T("\1\x80\0\0\xffy"x255, 1, 255, "y\0"x255"\0");
    T("\x80\0\0\xff\0"x255, 255, 0, x255"\0\0");
    T("\1\x80\0\1\0yx"x255, 1, 256, "y\0x"x255"\0");
    T("\x80\0\1\0\0x"x255, 256, 0, x255"x\0\0");
    T("\5\6./,';=./,';", 5, 6, "./,';\0=./,';\0");

    F("\0\0");
    F("\0\1x");
    F("\2\0x=");
    F("\2\0x\0");
    F("\0\2x\0");
    F("\0\2x\0");
    F("\x80\0\4\1\0");
    F("\0\x80\x10\0\1");
    return 0;
}
